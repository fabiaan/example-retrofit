package com.example.examenapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    List<Episodio> listaEpisodio;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaEpisodio = new ArrayList<>();

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        ApiInterface apiService = ApiCliente.obtenerCliente().create(ApiInterface.class);
        Call<List<Episodio>> call = apiService.obtenerListaEpisodios();

        call.enqueue(new Callback<List<Episodio>>() {
            @Override
            public void onResponse(Call<List<Episodio>> call, Response<List<Episodio>> response) {
                listaEpisodio = response.body();
                Log.d("TAG","Response = "+listaEpisodio);
                recyclerAdapter = new RecyclerAdapter(getApplicationContext(), listaEpisodio);
                recyclerView.setAdapter(recyclerAdapter);
            }

            @Override
            public void onFailure(Call<List<Episodio>> call, Throwable t) {
                Log.d("TAG","Response Error = "+t.toString());
            }
        });


    }
}