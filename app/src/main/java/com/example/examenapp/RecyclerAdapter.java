package com.example.examenapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> implements View.OnClickListener {

    Context context;
    List<Episodio> model;
    private View.OnClickListener listener;

    public RecyclerAdapter(Context context, List<Episodio> model) {
        this.context = context;
        this.model = model;
    }

    // Asignar el diseño de cada elemento de la lista
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(this.context);
        View view = inflater.inflate(R.layout.item, parent, false);
        view.setOnClickListener(this);

        return new MyViewHolder(view);
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    // Asignamos los elementos a la vista
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String nombre = model.get(position).getName();
        int numero = model.get(position).getNumber();
        String imagen = model.get(position).image.getMedium();

        holder.tv_nombre.setText(nombre);
        holder.tv_numero.setText(String.valueOf(numero));

        Glide.with(this.context)
                .load(imagen)
                .fitCenter()
                .centerCrop()
                .into(holder.iv_imagen);

    }

    @Override
    public int getItemCount() {
        int modelSize;
        if(model != null && !model.isEmpty()) {
            modelSize = model.size();
        }
        else {
            modelSize = 0;
        }

        return modelSize;
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick((view));
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_nombre, tv_numero;
        ImageView iv_imagen;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_nombre = itemView.findViewById(R.id.name);
            tv_numero = itemView.findViewById(R.id.number);
            iv_imagen = itemView.findViewById(R.id.imagen);
        }
    }
}
