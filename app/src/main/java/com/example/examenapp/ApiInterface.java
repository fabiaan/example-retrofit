package com.example.examenapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("shows/1/episodes")
    Call<List<Episodio>> obtenerListaEpisodios();
}
