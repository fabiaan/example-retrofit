package com.example.examenapp;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Episodio {

    @SerializedName("id")
    int id;

    @SerializedName("url")
    String url;

    @SerializedName("name")
    String name;

    @SerializedName("season")
    int season;

    @SerializedName("number")
    int number;

    @SerializedName("type")
    String type;

    @SerializedName("airdate")
    String airdate;

    @SerializedName("airtime")
    String airtime;

    @SerializedName("airstamp")
    String airstamp;

    @SerializedName("runtime")
    int runtime;

    @SerializedName("rating")
    Rating rating;

    @SerializedName("image")
    Image image;

    @SerializedName("summary")
    String summary;

    @SerializedName("_links")
    Links Links;


    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setSeason(int season) {
        this.season = season;
    }
    public int getSeason() {
        return season;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    public int getNumber() {
        return number;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setAirdate(String airdate) {
        this.airdate = airdate;
    }
    public String getAirdate() {
        return airdate;
    }

    public void setAirtime(String airtime) {
        this.airtime = airtime;
    }
    public String getAirtime() {
        return airtime;
    }

    public void setAirstamp(String airstamp) {
        this.airstamp = airstamp;
    }
    public String getAirstamp() {
        return airstamp;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }
    public int getRuntime() {
        return runtime;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
    public Rating getRating() {
        return rating;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    public Image getImage() {
        return image;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String getSummary() {
        return summary;
    }

    public void setLinks(Links Links) {
        this.Links = Links;
    }
    public Links getLinks() {
        return Links;
    }

}

class Rating {

    @SerializedName("average")
    double average;


    public void setAverage(double average) {
        this.average = average;
    }
    public double getAverage() {
        return average;
    }

}

class Image {

    @SerializedName("medium")
    String medium;

    @SerializedName("original")
    String original;


    public void setMedium(String medium) {
        this.medium = medium;
    }
    public String getMedium() {
        return medium;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
    public String getOriginal() {
        return original;
    }

}

class Self {

    @SerializedName("href")
    String href;


    public void setHref(String href) {
        this.href = href;
    }
    public String getHref() {
        return href;
    }

}

class Links {

    @SerializedName("self")
    Self self;


    public void setSelf(Self self) {
        this.self = self;
    }
    public Self getSelf() {
        return self;
    }

}